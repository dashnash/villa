import argparse
import boto3
import logging
import logging.handlers
import pprint
import sys
import time
from ouimeaux.environment import Environment

queue_url = 'https://sqs.us-west-2.amazonaws.com/492749601753/desk_lamp_toggle'
switch_name = 'Desk lamp'

def check_queue(poll = False):
    sqs_client = boto3.client('sqs')
    env = Environment()
    env.start()
    env.discover()
    logging.debug("SWITCHES: %s", env.list_switches())

    while(True):
        sqs_response = sqs_client.receive_message(QueueUrl=queue_url,
                                                  MaxNumberOfMessages=1,
                                                  MessageAttributeNames=[
                                                      'All'
                                                  ],
                                                  WaitTimeSeconds=20
                                                  )

        logging.debug(pprint.pformat(sqs_response))

        if 'Messages' in sqs_response:
            message_object = boto3.resource('sqs').Message(queue_url, sqs_response['Messages'][0]['ReceiptHandle'])

            # send toggle message
            logging.info('Toggling %s', switch_name)

            target_switch = env.get_switch(switch_name)
            target_switch.toggle()
            target_switch = None
            logging.info('Deleting message')
            message_object.delete()

        else:
            logging.info('No Messages, skipping')

        if not poll:
            break

if __name__ == "__main__":
    log_format = '%(asctime)s|%(levelname)s|%(name)s| %(message)s'

    parser = argparse.ArgumentParser('Polls queue for toggle commands')
    parser.add_argument('--poll', dest='poll', help='Enable continuous Polling', action='store_true')
    parser.add_argument('--log', dest='log_file', help='Log File Path')

    args = parser.parse_args()

    logging.getLogger('').setLevel(logging.INFO)

    if args.log_file:
        file_handler = logging.handlers.TimedRotatingFileHandler(args.log_file, backupCount=1, when='midnight')
        file_handler.setFormatter(logging.Formatter(log_format))
        logging.getLogger('').addHandler(file_handler)
    else:
        logging.basicConfig(format=log_format, stream=sys.stdout)


    if args.poll:
        logging.info('Continuous Polling')

    check_queue(args.poll)
    exit(0)
